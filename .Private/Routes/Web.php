<?php

use PHPRouter\RouteCollection;
use PHPRouter\Router;
use PHPRouter\Route;


Utils::Collection()->attachRoute(new Route('/', array(
    '_controller' => 'indexController::index',
    'methods' => 'GET'
)));

Utils::Collection()->attachRoute(new Route('/menu', array(
    '_controller' => 'menuController::index',
    'methods' => 'GET'
)));
