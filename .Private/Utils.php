<?php

use Monolog\Logger;
use Monolog\Handler\StreamHandler;
use PHPRouter\RouteCollection;
use PHPRouter\Router;
use PHPRouter\Route;

class Utils {
    public static $collection;

    public static function getCollections() {
        include('.Private/Routes/Web.php');
    }

    public static function loadControllers() {

        require_once('.Private/Controller.php');
        require_once('.Private/View.php');

        foreach (glob('.Private/Controllers/*.php') as $filename) {
            require_once($filename);
        }

    }

    public static function Render() {
        self::$collection = new RouteCollection();
        Utils::getCollections();
        $router = new Router(self::$collection);
        $router->setBasePath('/');
        $route = $router->matchCurrentRequest();
    }

    public static function Collection() {
        return self::$collection;
    }
}
