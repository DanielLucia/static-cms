<?php

    class View extends Smarty
    {

        public function __construct()
        {
            parent::__construct();

            $this->setTemplateDir('.Private/Views/');
            $this->setCompileDir('.Private/Views/Cache/Compilado');
            $this->setConfigDir('.Private/Views/Config/');
            $this->setCacheDir('.Private/Views/Cache/Cache');
            $this->muteExpectedErrors();
            //$this->debugging = DEBUG;
            //$this->addPluginsDir(DIR_SMARTY_PLUGIN);
        }

        public function render($template = false, $fetch = false)
        {
            $rutaView = '.Private/Views/' . ($template ? $template : 'index') . '.tpl';

            if (is_readable($rutaView)) {
                $this->assign('content', $rutaView);
                if ($fetch) {
                    return $this->fetch('.Private/Views/' . $template . '.tpl');
                } else {
                    $this->display('base.tpl');
                }

            }
        }
    }
